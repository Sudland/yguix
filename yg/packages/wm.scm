(define-module (yg packages wm)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system asdf)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system haskell)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system python)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages vulkan)
  #:use-module (yg packages vulkan)
  #:use-module (yg packages linux)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages check)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (yg packages freedesktop)
  #:use-module (yg packages xorg)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gl)
  #:use-module (yg packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lisp-check)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages man)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages time)
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (yg packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg))

(define-public swaylock-effects-latest
 (package
     (inherit swaylock-effects)
     (name "swaylock-effects")
     (version "1.7.0.0")
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/jirutka/swaylock-effects")
                     (commit "v1.7.0.0")))
               (file-name (git-file-name name version))
               (sha256
                (base32
                 "0cgpbzdpxj6bbpa8jwql1snghj21mhryyvj6sk46g66lqvwlrqbj"))
               (patches
                (parameterize
                 ((%patch-path
                    (map (lambda (directory)
                          (string-append directory "/yg/packages/patches"))
                     %load-path)))
                 (search-patches "swaylock-red-screen-fix.patch")))))
     (native-inputs (list pango pkg-config scdoc wayland-protocols-latest))
    (arguments
     (list #:configure-flags #~'("-Dsse=false")))))



(define-public waybar-wireplumber
  (package
    (inherit waybar)
    (name "waybar")
    (inputs (modify-inputs (package-inputs waybar)
              (prepend
                       wireplumber
                       pipewire)))))

(define-public wlroots-hyprland
  (package
    (inherit wlroots-latest)
    (name "wlroots")
    (version "3f0487d")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.freedesktop.org/wlroots/wlroots")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0f5vhh02jycsws25wil9zc7wd3b91kd3vxssqq5vswm9sbr4zyk1"))))))

(define-public swayfx
  (package
    (name "swayfx")
    (version "0.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/WillPower3309/swayfx")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0651gbkszc8wwkiiw983m3815cfyk4c9v4mpd1nqf27a0f6qjgsm"))))
    (build-system meson-build-system)
    (arguments
     `(;; elogind is propagated by wlroots -> libseat
       ;; and would otherwise shadow basu.
       #:configure-flags '("-Dsd-bus-provider=basu"
                           "-Db_lto=true")
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'hardcode-paths
           (lambda* (#:key inputs #:allow-other-keys)
             ;; Hardcode path to swaybg.
             (substitute* "sway/config.c"
               (("strdup..swaybg..")
                (string-append "strdup(\"" (assoc-ref inputs "swaybg")
                               "/bin/swaybg\")")))
             ;; Hardcode path to scdoc.
             (substitute* "meson.build"
               (("scdoc.get_pkgconfig_variable..scdoc..")
                (string-append "'" (assoc-ref inputs "scdoc")
                               "/bin/scdoc'")))
             #t)))))
    (inputs (list basu
                  vulkan-loader-latest
                  vulkan-headers-latest
                  cairo
                  gdk-pixbuf
                  json-c
                  libevdev
                  libinput-minimal-latest
                  libxkbcommon-latest
                  libdrm-latest
                  pcre2
                  pango
                  swaybg
                  wayland-latest
                  wlroots
                  scenefx))
    (native-inputs
     (list linux-pam mesa pkg-config scdoc wayland-protocols-latest pixman-latest))
    (home-page "https://github.com/swaywm/sway")
    (synopsis "Wayland compositor compatible with i3")
    (description "Sway is a i3-compatible Wayland compositor.")
    (license license:expat)))
(define-public sway
  (package
    (name "sway")
    (version "1.8.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/swaywm/sway")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1y7brfrsjnm9gksijgnr6zxqiqvn06mdiwsk5j87ggmxazxd66av"))))
    (build-system meson-build-system)
    (arguments
     `(;; elogind is propagated by wlroots -> libseat
       ;; and would otherwise shadow basu.
       #:configure-flags '("-Dsd-bus-provider=basu"
                           "-Db_lto=true")
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'hardcode-paths
           (lambda* (#:key inputs #:allow-other-keys)
             ;; Hardcode path to swaybg.
             (substitute* "sway/config.c"
               (("strdup..swaybg..")
                (string-append "strdup(\"" (assoc-ref inputs "swaybg")
                               "/bin/swaybg\")")))
             ;; Hardcode path to scdoc.
             (substitute* "meson.build"
               (("scdoc.get_pkgconfig_variable..scdoc..")
                (string-append "'" (assoc-ref inputs "scdoc")
                               "/bin/scdoc'")))
             #t)))))
    (inputs (list basu
                  vulkan-loader-latest
                  vulkan-headers-latest
                  cairo
                  gdk-pixbuf
                  json-c
                  libevdev
                  libinput-minimal-latest
                  libxkbcommon-latest
                  libdrm-latest
                  pcre2
                  pango
                  swaybg
                  wayland-latest
                  wlroots-latest))
    (native-inputs
     (list linux-pam mesa pkg-config scdoc wayland-protocols-latest pixman-latest))
    (home-page "https://github.com/swaywm/sway")
    (synopsis "Wayland compositor compatible with i3")
    (description "Sway is a i3-compatible Wayland compositor.")
    (license license:expat)))


(define-public hyprland
  (package
    (name "hyprland")
    (version "0.33.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/hyprwm/Hyprland")
             (commit (string-append "v"version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1b4d6r68cqx09blf5xdr1c23hw7axr8sx21wsxjjygqrhkkabdx7"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f ; no check target
       #:configure-flags
       (list "-DNO_SYSTEMD=1")
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'hardcode-paths
           (lambda* (#:key inputs #:allow-other-keys)
             (copy-recursively (assoc-ref %build-inputs "wlroots") "subprojects/wlroots")
             (copy-recursively (assoc-ref %build-inputs "wlroots") "subprojects/wlroots/build")
             (substitute* "CMakeLists.txt"
              (("OpenGL::EGL") "")
              (("subprojects/hyprland-protocols") (string-append "" (assoc-ref inputs "hyprland-protocols" ) "/share/hyprland-protocols"))))))))
    (native-inputs
     (list pkg-config gcc-12))
    (inputs
     `(("wayland",wayland-latest)
       ("wayland-protocols",wayland-protocols-latest)
       ("cairo",cairo)
       ("pango",pango)
       ("libdrm-latest",libdrm)
       ("egl-wayland",egl-wayland)
       ("libxkbcommon", libxkbcommon)
       ("libinput", libinput)
       ("hyprland-protocols",hyprland-protocols)
       ("wlroots" ,wlroots-hyprland)))

    (home-page "https://www.hyprland.org/")
    (synopsis "Dynamic tiling Wayland compositor based on wlroots")
    (description
     "Hyprland is a dynamic tiling Wayland compositor based on wlroots
that doesn't sacrifice on its looks. It supports multiple layouts,
fancy effects, has a very flexible IPC model allowing for a lot of
customization, and more. ")
    (license license:bsd-3)))

(define-public scenefx
  (package
    (name "scenefx")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/wlrfx/scenefx")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1r7f8bprsn0mwlkmc8d14nr3iibljfyxypb4i06v66ghlngaw6dw"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config gcc-12 cmake))
    (inputs (list
              basu
              wayland
              wlroots))


    (home-page "https://github.com/wlrfx/scenefx")
    (synopsis "Dynamic tiling Wayland compositor based on wlroots")
    (description "A drop-in replacement for the wlroots scene API that allows wayland compositors to render surfaces with eye-candy effects ")
    (license license:expat)))  ; MIT license
