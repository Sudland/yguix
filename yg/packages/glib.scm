(define-module (yg packages glib)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix utils)
  #:use-module (yg packages glib)
  #:use-module (yg packages virtualization)
  #:use-module (guix git-download))

(define-public libglibutil
  (package
    (name "libglibutil")
    (version "1.0.75")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/sailfishos/libglibutil")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0bhc6y9xzhydahbpqf104asdyj91s2zg3icf9m4ic8n12n0vjwk0"))))
    (build-system gnu-build-system)
    (arguments
     `(#:make-flags (list (string-append "CC=" ,(cc-for-target))
                          (string-append "DESTDIR=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'remove-usr-prefix
           (lambda* _
             (substitute* "libglibutil.pc.in"
               (("/usr/include") (string-append %output "/include")))
             (substitute* "Makefile"
               (("usr/") ""))))
         (add-after 'install 'install-dev
           (lambda* _
             (invoke "make" "install-dev" (string-append "DESTDIR=" %output))))
         (replace 'check
           (lambda* (#:key tests? #:allow-other-keys)
             (when tests?
               (chdir "test")
               (invoke "make" (string-append "CC=" ,(cc-for-target)))
               (chdir "..") #t))))))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (inputs `(("glib" ,glib)))
    (home-page "https://git.sailfishos.org/mer-core/libglibutil")
    (synopsis "GLib utilites")
    (description "This package provides library of glib utilities.")
    (license license:bsd-3)))


(define-public libgbinder
  (package
    (name "libgbinder")
    (version "1.1.35")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/mer-hybris/libgbinder")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "11asby5grv3aggnd504gv7vhcbyinvffr29dmwq768b9r5nw8a8s"))))
    (build-system gnu-build-system)
    (arguments
     `(#:make-flags (list (string-append "CC=" ,(cc-for-target))
                          (string-append "DESTDIR=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'fix-pkg-config-in
           (lambda* _
             (substitute* "Makefile"
               (("usr/") ""))
             (substitute* "libgbinder.pc.in"
               (("@libdir@") (string-append (assoc-ref %outputs "out") "/lib"))
               (("/usr/include") (string-append %output "/include")))))
         (add-after 'install 'install-dev
           (lambda* _
             (invoke "make" "install-dev" (string-append "DESTDIR=" %output))))
         (replace 'check
           (lambda* (#:key tests? #:allow-other-keys)
             (when tests?
               (chdir "test")
               (invoke "make" (string-append "CC=" ,(cc-for-target)))
               (chdir "..") #t))))))
    (native-inputs `(("bison" ,bison)
                     ("flex" ,flex)
                     ("pkg-config" ,pkg-config)))
    (inputs `(("glib" ,glib)
              ("libglibutil" ,libglibutil)))
    (home-page "https://github.com/mer-hybris/libgbinder")
    (synopsis "GLib-style interface to binder")
    (description "This package provides GLib-style interface to binder:
@enumerate
@item Integration with GLib event loop
@item Detection of 32 vs 64 bit kernel at runtime
@item Asynchronous transactions that don't block the event thread
@item Stable service manager and low-level transation APIs
@end enumerate")
    (license license:bsd-3)))



(define-public python-gbinder
  (package
    (name "python-gbinder")
    (version "1.1.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/erfanoabdi/gbinder-python")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1qxadb1ccmdwnwj4p15sm2i3b7cjaxh7far1jk9sxrblwh7497ds"))))
    (build-system python-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda* _
             (invoke "python" "setup.py" "build_ext" "--inplace" "--cython"))))))
    (native-inputs `(("python-cython" ,python-cython)
                     ("pkg-config" ,pkg-config)))
    (inputs `(("glib" ,glib)
              ("libgbinder" ,libgbinder)
              ("libglibutil" ,libglibutil)))
    (home-page "https://github.com/erfanoabdi/gbinder-python")
    (synopsis "Python bindings for libgbinder")
    (description "This package provides Python bindings for libgbinder.")
    (license license:gpl3+)))
