(define-module (yg packages shells)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix svn-download)
  #:use-module (gnu packages commencement)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix build utils)
  #:use-module (ice-9 popen)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system cmake)
  #:use-module (guix deprecation)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages databases))

(define-public fish-command-timer
  (package
    (name "fish-command-timer")
    (version "0.1.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/jichu4n/fish-command-timer")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0v1ddcqchc1rbkv6y4jl2i9bnk4ynyp6wicnciddk60zkmjsr500"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("conf.d" "conf.d"))))
    (home-page "https://github.com/jichu4n/fish-command-timer")
    (synopsis "fish-command-timer")
    (description "FishShell extension for printing timing information for each command executed.")
    (license license:expat)))

(define-public fzf.fish
  (package
    (name "fzf.fish")
    (version "10.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/PatrickF1/fzf.fish")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0pf0nwqiaf13cp41c4cj4f4lpsig4zkizsxhc0yqfyq78lm0pwyp"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("conf.d" "conf.d")
         ("functions" "functions")
         ("completions" "completions"))))
    (home-page "https://github.com/PatrickF1/fzf.fish")
    (synopsis "fzf functions for Fish shell")
    (description "Fzf plugin for Fish")
    (license license:expat)))
