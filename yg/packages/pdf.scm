(define-module (yg packages pdf)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages djvu)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages javascript)
  #:use-module (gnu packages lesstif)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages man)
  #:use-module (gnu packages markup)
  #:use-module (gnu packages ocr)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages time)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (srfi srfi-1))

(define-public sioyek
  (package
    (name "sioyek")
    (version "v2.0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/ahrm/sioyek/archive/refs/tags/"
                    version ".tar.gz"))
              (sha256
               (base32
                "05yznpzals988gllmfqpphl6z14l45b6ng6d4agwb5z2lmnqnfcj"))))
    (build-system qt-build-system)
    (native-inputs (list pkg-config))
    (inputs (list qtbase-5
                  harfbuzz
                  jbig2dec
                  mupdf
                  openjpeg
                  qt3d-5
                  zlib))

    (arguments
     `(#:tests? #f ;no tests
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'remove-libmupdfthird.a-requirement
                    (lambda _
                      ;; Ignore a missing (apparently superfluous) static library.
                      (substitute* "pdf_viewer_build_config.pro"
                        (("-lmupdf-third")
                         "")
                        (("-lmupdf-threads")
                         "")
                        (("-ljpeg")
                         "")
                        (("-lfreetype")
                         "")
                        (("-lmujs")
                         "")
                        (("-lgumbo")
                         ""))))
                  (replace 'configure
                    (lambda* (#:key outputs #:allow-other-keys)
                      ;; (substitute* "sioyek.pri"
                      ;; (("/usr") (assoc-ref outputs "out"))
                      (invoke "qmake" "CONFIG+=thread"
                              (string-append "PREFIX="
                                             (assoc-ref outputs "out"))
                              "pdf_viewer_build_config.pro"))))))
    (home-page "https://sioyek.info/")
    (synopsis "PDF research paper reader")
    (description
     "Sioyek is a PDF viewer with a focus on textbooks and research papers.")
    (license license:gpl3+)))
