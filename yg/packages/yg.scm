(define-module (yg packages yg)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix svn-download)
  #:use-module (gnu packages commencement)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix build utils)
  #:use-module (ice-9 popen)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system cmake)
  #:use-module (guix deprecation)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages databases))


(define-public norway-power
  (package
    (name "norway-power")
    (version "0.1")
    (outputs '("out"))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/Sudland/norway-power-guile")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "13j4wg4lnpidc1wwrlhqfirw72n6g0v7nfq6f0sxjv1cb9s25bj3"))))
    (build-system gnu-build-system)
    (arguments
     `(#:modules ((guix build utils)
                  (guix build gnu-build-system)
                  (ice-9 rdelim)
                  (ice-9 popen))
       #:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (delete 'build)
           (delete 'check)
           (add-before 'install 'add-shebang
              (lambda* (#:key inputs #:allow-other-keys)
                (substitute* "norway-power.scm"
                  (("\\(use-modules")
                   (string-append "#!" (assoc-ref inputs "guile") "/bin/guile \n!#\n\(use-modules")))))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (install-file "norway-power.scm"
                               (string-append out "/bin")))))
           (add-after 'install 'wrap-program
              (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (json (assoc-ref inputs "guile-json"))
                      (curl (assoc-ref inputs "guile-curl"))
                      (deps (list json curl))
                      (guile (assoc-ref inputs "guile"))
                      (effective
                       (read-line
                        (open-pipe* OPEN_READ
                                    (string-append guile "/bin/guile")
                                    "-c" "(display (effective-version))")))


                      (mods
                       (string-drop-right  ;drop trailing colon
                        (string-join deps
                                     (string-append "/share/guile/site/"
                                                    effective ":")
                                     'suffix)
                        1))
                      (objs
                       (string-drop-right
                        (string-join deps
                                     (string-append "/lib/guile/" effective
                                                    "/site-ccache:")
                                     'suffix)
                        1)))
                 (wrap-program (string-append out "/bin/norway-power.scm")
                  `("PATH" ":" prefix (,(string-append out "/bin")))
                  `("GUILE_LOAD_PATH" ":" prefix (,mods))
                  `("GUILE_LOAD_COMPILED_PATH" ":" prefix (,objs)))))))))
    (inputs
     (list guile-3.0-latest
           guile-json-4
           guile-curl))
    (native-inputs
     (list autoconf automake pkg-config texinfo ephemeralpg))

    (home-page "https://gitlab.com/Sudland/norway-power-guile")
    (synopsis "A simple GNU Guile script that fetches and display the current price.")
    (description "A simple GNU Guile script that fetches and display the current price.")
    (license license:gpl3)))
