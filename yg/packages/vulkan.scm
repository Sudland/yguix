(define-module (yg packages vulkan)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages wine)
  #:use-module (gnu packages xorg))


(define-public vulkan-headers-latest
  (package
    (inherit vulkan-headers)
    (name "vulkan-headers")
    (version "1.3.246")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KhronosGroup/Vulkan-Headers")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
          "1q4n81qrpr3x0rhh7r9r76jzn3a6qzi1q21dds9z5xc1hxz6kkdm"))))))

(define-public vulkan-loader-latest
  (package
    (inherit vulkan-loader)
    (name "vulkan-loader")
    (version "1.3.246")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KhronosGroup/Vulkan-Loader")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
          "04kknm84h0069rmc153b4hqgcr8fc3lnbq3b97xzn562ygspvams"))))
    (inputs
     (list vulkan-headers-latest))
    (arguments
     `(#:configure-flags
       ,#~(list
           (string-append "-DVULKAN_HEADERS_INSTALL_DIR="
                          #$(this-package-input "vulkan-headers"))
           (string-append "-DCMAKE_INSTALL_INCLUDEDIR="
                          #$(this-package-input "vulkan-headers")
                          "/include"))
       #:tests? #f))))                    ;no tests
