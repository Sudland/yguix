(define-module (yg packages docker)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public distrobox
  (package
   (name "distrobox")
   (version "1.5.0.2")
   (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/89luca89/distrobox")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
         (base32 "0h6rpgbcmg33vriyzh9nkdxj8yhfn0y35i1wh1zmb7zss3ik9kxj"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan
      '(("distrobox" "bin/")
        ("distrobox-enter" "bin/")
        ("distrobox-create" "bin/")
        ("distrobox-ephemeral" "bin/")
        ("distrobox-export" "bin/")
        ("distrobox-generate-entry" "bin/")
        ("distrobox-host-exec" "bin/")
        ("distrobox-init" "bin/")
        ("distrobox-list" "bin/")
        ("distrobox-rm" "bin/")
        ("distrobox-stop" "bin/")
        ("distrobox-upgrade" "bin/")
        ("distrobox-assemble" "bin/")
        ("man/man1/" "share/man/man1"))))
   (home-page "https://github.com/89luca89/distrobox")
   (synopsis "Run any distro with docker or podman")
   (description "Use any Linux distribution inside your terminal. Enable both backward and forward compatibility with software and freedom to use whatever distribution you’re more comfortable with. Distrobox uses podman or docker to create containers using the Linux distribution of your choice. The created container will be tightly integrated with the host, allowing sharing of the HOME directory of the user, external storage, external USB devices and graphical apps (X11/Wayland), and audio.")
   (license license:gpl3)))
