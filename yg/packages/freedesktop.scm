(define-module (yg packages freedesktop)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system asdf)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system haskell)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system python)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module (yg packages linux)
  #:use-module (yg packages gl)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages samba)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages check)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lisp-check)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages man)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages time)
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (yg packages xdisorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (srfi srfi-1)
  #:use-module (gnu packages xorg))

(define-public libinput-latest
  ;; Updating this will rebuild over 700 packages through libinput-minimal.
  (package
    (name "libinput")
    (version "1.22.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://gitlab.freedesktop.org/libinput/libinput/-/archive/" version "/" name "-" version ".tar.gz"))
              (sha256
               (base32
                "11qyw6dx9naydyzfqip4vahb2fckmr2gg9vsafvl7hy32qyf1na5"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags '("-Ddocumentation=false")

       ;; XXX: Using 'debug' or 'debugoptimized' pulls in an additional test that
       ;; hangs, and the comments around it suggests that we should be using this
       ;; Meson target anyway.
       #:build-type "release"))
    (native-inputs
     (list check pkg-config))
    (inputs
     (list cairo
           glib
           gtk+
           libevdev
           libwacom
           mtdev))
    (propagated-inputs
     `(;; libinput.h requires <libudev.h>, so propagate it.
       ("udev" ,eudev)))
    (home-page "https://www.freedesktop.org/wiki/Software/libinput/")
    (synopsis "Input devices handling library")
    (description
     "Libinput is a library to handle input devices for display servers and
other applications that need to directly deal with input devices.")
    (license license:x11)))


(define-public wayland-latest
  (package
    (name "wayland")
    (version "1.22.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://gitlab.freedesktop.org/wayland/wayland/-/releases/"
                                  version "/downloads/" name "-" version ".tar.xz"))
              (sha256
               (base32
                "0hjq8b1cprxpxcv2klgir1hd63vy5hrqilp9v317394qlqgayh0m"))))
    (build-system meson-build-system)
    (outputs '("out" "doc"))
    (arguments
     `(#:parallel-tests? #f
        #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-docbook-xml
           (lambda* (#:key native-inputs inputs #:allow-other-keys)
             (with-directory-excursion "doc"
               (substitute* (find-files "." "\\.xml$")
                 (("http://www.oasis-open.org/docbook/xml/4\\.5/")
                  (string-append (assoc-ref (or native-inputs inputs)
                                            "docbook-xml")
                                 "/xml/dtd/docbook/"))
                 (("http://www.oasis-open.org/docbook/xml/4\\.2/")
                  (string-append (assoc-ref (or native-inputs inputs)
                                            "docbook-xml-4.2")
                                 "/xml/dtd/docbook/"))))))
         (add-after 'install 'move-doc
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (doc (assoc-ref outputs "doc")))
               (mkdir-p (string-append doc "/share"))
               (rename-file
                (string-append out "/share/doc")
                (string-append doc "/share/doc"))))))))
    (native-inputs
     `(("docbook-xml-4.2" ,docbook-xml-4.2)
       ("docbook-xml" ,docbook-xml)
       ("docbook-xsl" ,docbook-xsl)
       ("dot" ,graphviz)
       ("doxygen" ,doxygen)
       ("pkg-config" ,pkg-config)
       ("python" ,python)
       ("xmlto" ,xmlto)
       ("xsltproc" ,libxslt)
       ,@(if (%current-target-system)
             `(("pkg-config-for-build" ,pkg-config-for-build)
               ("wayland" ,this-package)) ; for wayland-scanner
             '())))
    (inputs
     (list expat libxml2))           ; for XML_CATALOG_FILES
    (propagated-inputs
     (list libffi))
    (home-page "https://wayland.freedesktop.org/")
    (synopsis "Core Wayland window system code and protocol")
    (properties
     '((release-monitoring-url
        . "https://wayland.freedesktop.org/releases.html")))
    (description "Wayland is a project to define a protocol for a compositor to
talk to its clients as well as a library implementation of the protocol.  The
compositor can be a standalone display server running on Linux kernel
modesetting and evdev input devices, an X application, or a wayland client
itself.  The clients can be traditional applications, X servers (rootless or
fullscreen) or other display servers.")
    (license license:expat)))

(define-public wayland-protocols-latest
  (package
    (name "wayland-protocols")
    (version "1.32")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://gitlab.freedesktop.org/wayland/wayland-protocols/-/releases/" version "/downloads/"
                    "wayland-protocols-" version ".tar.xz"))
              (sha256
               (base32
                "04dsn79409mryxs6maq9kfhca97gvl3pr1ggjnv9d0hc6jfpjnbl"))))
    (build-system meson-build-system)
    (inputs
     (list wayland-latest))
    (native-inputs
     (list pkg-config python))
    (synopsis "Wayland protocols")
    (description "Wayland-Protocols contains Wayland protocols that add
functionality not available in the Wayland core protocol.  Such protocols either
add completely new functionality, or extend the functionality of some other
protocol either in Wayland core, or some other protocol in wayland-protocols.")
    (home-page "https://wayland.freedesktop.org")
    (properties
     '((release-monitoring-url
        . "https://wayland.freedesktop.org/releases.html")))
    (license license:expat)))

(define-public libliftoff
  (package
    (name "libliftoff")
    (version "0.4.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.freedesktop.org/emersion/libliftoff")
             (commit (string-append "v"version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1ikjp638d655ycaqkdnzhb12d29kkbb3a46lqhbhsfc8vsqj3z1l"))))
    (build-system meson-build-system)
    (inputs
     (list libdrm-latest))
    (native-inputs
     (list pkg-config))
    (synopsis "Lightweight KMS plane library.")
    (description "libliftoff eases the use of KMS planes from userspace without standing in your
way. Users create \"virtual planes\" called layers, set KMS properties on them,
and libliftoff will pick hardware planes for these layers if possible.")
    (home-page "https://gitlab.freedesktop.org/emersion/libliftoff")
    (license license:expat)))

(define-public hyprland-protocols
  (package
    (name "hyprland-protocols")
    (version "0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/hyprwm/hyprland-protocols")
             (commit (string-append "v"version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1drjznj7fn6m5m6skhzh0p031cb5x0bb4i56jxnxwpwaa71g1z20"))))
    (build-system meson-build-system)
    (inputs
     (list wayland-latest))
    (native-inputs
     (list pkg-config python))
    (synopsis "Hyprland protocols")
    (description "Wayland protocol extensions for Hyprland.

This repository exists in an effort to bridge the gap between Hyprland and KDE/Gnome's functionality.

Since wlr-protocols is closed for new submissions, and wayland-protocols is very slow with changes, this repo will hold protocols used by Hyprland to bridge the aforementioned gap.
      ")
    (home-page "https://github.com/hyprwm/hyprland-protocols")
    (license license:bsd-3)))


(define-public xdg-desktop-portal-yg
  (package
    (name "xdg-desktop-portal-yg")
    (version "1.16.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/flatpak/xdg-desktop-portal/releases/download/"
                    version "/xdg-desktop-portal-" version ".tar.xz"))
              (sha256
               (base32
                "06cczlh39kc41rvav06v37sad827y61rffy3v29i918ibj8sahav"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("autoconf" ,autoconf)
       ("automake" ,automake)
       ("libtool" ,libtool)
       ("glib:bin" ,glib "bin")
       ("which" ,which)
       ("gettext" ,gettext-minimal)))
    (inputs
     `(("gdk-pixbuf" ,gdk-pixbuf)
       ("glib" ,glib)
       ("flatpak" ,flatpak)
       ("fontconfig" ,fontconfig)
       ("json-glib" ,json-glib)
       ("libportal" ,libportal)
       ("dbus" ,dbus)
       ("geoclue" ,geoclue)
       ("pipewire" ,pipewire)
       ("fuse" ,fuse)))
    (arguments
     `(#:configure-flags
       (list "--with-systemd=no")
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'po-chmod
           (lambda _
             ;; Make sure 'msgmerge' can modify the PO files.
             (for-each (lambda (po)
                         (chmod po #o666))
                       (find-files "po" "\\.po$"))))
         (add-after 'unpack 'set-home-directory
           (lambda _ (setenv "HOME" "/tmp"))))))
    (native-search-paths
     (list (search-path-specification
            (variable "XDG_DESKTOP_PORTAL_DIR")
            (separator #f)
            (files '("share/xdg-desktop-portal/portals")))))
    (home-page "https://github.com/flatpak/xdg-desktop-portal")
    (synopsis "Desktop integration portal for sandboxed apps")
    (description
     "xdg-desktop-portal is a @dfn{portal front-end service} for Flatpak and
possibly other desktop containment frameworks.  It works by exposing a series
of D-Bus interfaces known as portals under a well-known
name (@code{org.freedesktop.portal.Desktop}) and object
path (@code{/org/freedesktop/portal/desktop}).

The portal interfaces include APIs for file access, opening URIs, printing
and others.")
    (license license:lgpl2.1+)))

(define-public xdg-desktop-portal-wlr-yg
  (package
    (name "xdg-desktop-portal-wlr-yg")
    (version "0.7.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/emersion/xdg-desktop-portal-wlr")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1b3hpp3ybjgnnmnwsyb5bsnvz9q5nr3zz0j1alh02g24f68lf00k"))
              (patches (search-patches "xdg-desktop-portal-wlr-harcoded-length.patch"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags
       '("-Dsystemd=disabled"
         "-Dsd-bus-provider=libelogind")
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'hardcode-binaries
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((sh (search-input-file inputs "/bin/sh"))
                   (grim (search-input-file inputs "/bin/grim"))
                   (slurp (search-input-file inputs "/bin/slurp")))
               (substitute* "src/screenshot/screenshot.c"
                 (("grim") grim)
                 (("slurp") slurp)
                 (("execl\\(\"/bin/sh\", \"/bin/sh\"")
                  (string-append "execl(\"" sh "\", \"" sh "\"")))
               (substitute* "src/screencast/screencast.c"
                 (("execvp\\(\"sh")
                  (string-append "execvp(\"" sh)))))))))
    (native-inputs
     (list cmake pkg-config mesa libdrm-latest))
    (inputs (list elogind
                  bash-minimal
                  grim
                  iniparser
                  libinih
                  pipewire
                  slurp
                  wayland-latest
                  wayland-protocols-latest))
    (home-page "https://github.com/emersion/xdg-desktop-portal-wlr")
    (synopsis "@code{xdg-desktop-portal} backend for wlroots")
    (description
     "This package provides @code{xdg-desktop-portal-wlr}.  This project
seeks to add support for the screenshot, screencast, and possibly
remote-desktop @code{xdg-desktop-portal} interfaces for wlroots based
compositors.")
    (license license:expat)))

(define-public libinput-minimal-latest
  (package/inherit libinput-latest
    (name "libinput-minimal")
    (inputs
     (fold alist-delete (package-inputs libinput)
           '("cairo" "glib" "gtk+" "libwacom")))
    (arguments
     (substitute-keyword-arguments (package-arguments libinput)
      ((#:configure-flags flags ''())
       `(cons* "-Dlibwacom=false"
               "-Ddebug-gui=false"    ;requires gtk+@3
               ,flags))))))
