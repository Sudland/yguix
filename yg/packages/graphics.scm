(define-module (yg packages graphics)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix build-system scons)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages man)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages terminals)
  #:use-module (yg packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages)
  #:use-module (ice-9 match))


(define-public vkbasalt
  (package
    (name "vkbasalt")
    (version "0.3.2.10")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/DadSchoorse/vkBasalt")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0f3qmcmqnnh8i9qd2sd3p5w0akn8rkzfm5z0hc0wazgci4lqjbhq"))))
    (build-system meson-build-system)
    (arguments
     (list
      #:build-type "release"
      #:configure-flags
      #~(list "-Dappend_libdir_vkbasalt=true")))
    (inputs
     (list
           spirv-headers
           glslang
           pkg-config
           mesa
           mesa-utils
           dbus
           libx11
           vulkan-headers
           ))
    (home-page "https://github.com/DadSchoorse/vkBasalt/pulls")
    (synopsis "vkBasalt is a Vulkan post processing layer to enhance the visual graphics of games.")
    (description "A Vulkan post-processing layer. Some of the effects are CAS, FXAA, SMAA, deband.")
    (license license:zlib)))

(define-public reshade-shaders
  (package
    (name "reshade-shaders")
    (version "9999")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/crosire/reshade-shaders")
             (commit "a2314cbf9a6c96de4d434b2f9ab8fa9265b8e575" )))
        (file-name (git-file-name name version))
       (sha256
        (base32 "07lvybc8x1633ynsq0z23hkq6jg97k339sh6fd0d8kk8n123z1xw"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("Shaders" "share/reshade-shaders/")
         ("Textures" "share/reshade-shaders/"))))
    (home-page "https://reshade.me https://github.com/crosire/reshade-shaders")
    (synopsis "This repository aims to collect post-processing shaders written in the ReShade FX shader language.")
    (description "A collection of post-processing shaders written for ReShade.")
    (license license:bsd-3)))
