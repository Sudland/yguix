(define-module (yg packages linux)
  #:use-module (guix build-system meson)
  #:use-module (guix build utils)
  #:use-module  (guix git-download)
  #:use-module  (guix gexp)
  #:use-module  ((guix licenses) #:prefix license:)
  #:use-module  (guix packages)
  #:use-module  (gnu packages linux)
  #:use-module  (gnu packages pkg-config)
  #:use-module  (gnu packages freedesktop)
  #:use-module  (gnu packages avahi)
  #:use-module  (gnu packages audio)
  #:use-module  (gnu packages lua)
  #:use-module  (gnu packages pulseaudio)
  #:use-module  (gnu packages glib)
  #:use-module  (yg packages vulkan)
  #:use-module  (yg packages xdisorg)
  #:use-module  (gnu packages vulkan)
  #:use-module  (gnu packages build-tools)
  #:use-module  (gnu packages libusb)
  #:use-module  (ice-9 match))

(define-public wireplumber-latest
  (package
    (name "wireplumber")
    (version "0.4.17")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url
              "https://gitlab.freedesktop.org/pipewire/wireplumber.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "00jzn0pxy2ws819yg4p8xxhngqai3labd1alaxb8zwzymr7m06my"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags '("-Dsystemd=disabled"
                           "-Dsystem-lua=true")))
    (native-inputs
     (list `(,glib "bin")
           pkg-config))
    (inputs (list dbus elogind glib lua pipewire-latest))
    (home-page "https://gitlab.freedesktop.org/pipewire/wireplumber")
    (synopsis "Session / policy manager implementation for PipeWire")
    (description "WirePlumber is a modular session / policy manager for
PipeWire and a GObject-based high-level library that wraps PipeWire's API,
providing convenience for writing the daemon's modules as well as external
tools for managing PipeWire.")
    (license license:expat)))

(define-public pipewire-latest
  (package
    (inherit pipewire)
    (name "pipewire")
    (version "1.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/PipeWire/pipewire")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0bs7nxpzxyhm0pjyskq9zq7rfjiibjw862xsmbf34330jjqz9xxf"))))
    (build-system meson-build-system)
    (arguments
      (list
       #:meson meson
       #:configure-flags
       #~(list (string-append "-Dudevrulesdir=" #$output "/lib/udev/rules.d")
               "-Dsystemd=disabled"
               "-Db_lto=true"
               "-Dpipewire-jack=disabled"
               "-Djack=disabled"
               "-Dbluez5-backend-ofono=disabled"
               "-Dlibusb=enabled"
               "-Dvulkan=enabled"
               "-Dbluez5=enabled"
               "-Dspa-plugins=enabled"
               "-Dsession-managers=[]")))
    (native-inputs
     (list `(,glib "bin")
           pkg-config))
    (inputs (modify-inputs (package-inputs pipewire)
              (prepend avahi
                       libusb
                       libdrm-latest
                       bluez
                       jack-2
                       ldacbt
                       pulseaudio
                       vulkan-loader-latest
                       vulkan-headers)))))
