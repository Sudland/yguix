(define-module (yg packages gl)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages video)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xdisorg)
  #:use-module (yg packages xdisorg)
  #:use-module (yg packages freedesktop)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix build-system waf)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1)
                #:hide (zip)))

(define gcc-toolchain*
  (delay (module-ref (resolve-interface '(gnu packages commencement))
                     'gcc-toolchain-10)))

(define libva-without-mesa
  ;; Delay to work around circular import problem.
  (delay (package
           (inherit libva)
           (name "libva-without-mesa")
           (inputs `(,@(fold alist-delete
                             (package-inputs libva)
                             '(mesa wayland-latest))))
           (arguments
            (strip-keyword-arguments '(#:make-flags)
                                     (substitute-keyword-arguments (package-arguments
                                                                    libva)
                                       ((#:configure-flags flags)
                                        '(list "--disable-glx" "--disable-egl"))))))))


(define-public libepoxy-latest
  (package
    (name "libepoxy")
    (version "1.5.10")
    (home-page "https://github.com/anholt/libepoxy")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url home-page)
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0jw02bzdwynyrwsn5rhcacv92h9xx928j3xp436f8gdnwlyb5641"))))
    (arguments
     (list #:phases #~(modify-phases %standard-phases
                        (add-before 'configure 'patch-paths
                          (lambda* (#:key inputs #:allow-other-keys)
                            (let ((mesa (dirname (search-input-file inputs
                                                  "lib/libGL.so"))))
                              (substitute* (find-files "." "\\.[ch]$")
                                (("libGL.so.1")
                                 (string-append mesa "/libGL.so.1"))
                                (("libEGL.so.1")
                                 (string-append mesa "/libEGL.so.1")))))))))
    (build-system meson-build-system)
    (native-inputs (list pkg-config python))
    (propagated-inputs
     ;; epoxy.pc: 'Requires.private: gl egl'
     (list mesa))
    (synopsis "Library for handling OpenGL function pointer management")
    (description "A library for handling OpenGL function pointer management.")
    (license license:x11)))

(define-public llvm-for-mesa
  ;; Note: update the 'clang' input of mesa-opencl when bumping this.
  (let ((base-llvm llvm-15))
    (package
      (inherit base-llvm)
      (arguments
       (substitute-keyword-arguments (package-arguments base-llvm)
         ((#:modules modules
           '((guix build cmake-build-system)
             (guix build utils)))
          `((ice-9 regex)
            (srfi srfi-1)
            (srfi srfi-26)
            ,@modules))
         ((#:configure-flags cf
           ''())
          #~(cons*
             ;; AMDGPU is needed by the vulkan drivers.
             #$(string-append "-DLLVM_TARGETS_TO_BUILD="
                              (system->llvm-target) ";AMDGPU")
             ;; Tools and utils decrease the output by ~100 MiB.
             "-DLLVM_BUILD_TOOLS=NO"
             (remove (cut string-match
                          "-DLLVM_(TARGETS_TO_BUILD|INSTALL_UTILS).*" <>)
                     #$cf)))
         ((#:phases phases
           '%standard-phases)
          #~(modify-phases #$phases
              (add-after 'install 'delete-static-libraries
                ;; If these are just relocated then llvm-config can't find them.
                (lambda* (#:key outputs #:allow-other-keys)
                  (for-each delete-file
                            (find-files (string-append (assoc-ref outputs
                                                                  "out")
                                                       "/lib") "\\.a$"))))
              (add-after 'install 'build-and-install-llvm-config
                (lambda* (#:key outputs #:allow-other-keys)
                  (let ((out (assoc-ref outputs "out")))
                    (substitute* "tools/llvm-config/CMakeFiles/llvm-config.dir/link.txt"
                      (((string-append "/tmp/guix-build-llvm-"
                                       #$(package-version base-llvm)
                                       ".drv-0/build/lib"))
                       (string-append out "/lib")))
                    (invoke "make" "llvm-config")
                    (install-file "bin/llvm-config"
                                  (string-append out "/bin")))))))))
      ;; We don't override the name so we hide it so we don't have package
      ;; ambiguities from the CLI.
      (properties `((hidden? . #t) ,@(package-properties base-llvm))))))
