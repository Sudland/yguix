
(define-module (yg packages xdisorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix build-system scons)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages man)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages)
  #:use-module (ice-9 match))

(define-public libxkbcommon-latest
  (package
    (inherit libxkbcommon)
    (name "libxkbcommon")
    (version "1.6.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://xkbcommon.org/download/libxkbcommon-"
                                  version ".tar.xz"))
              (sha256
               (base32
                "0awwz5pg9x5bj0d7dpg4a7bd4gl6k55mlpxwb12534fkrpn19p0f"))))))

(define-public pixman-latest
  (package
    (inherit pixman)
    (name "pixman")
    (version "0.43.0")
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags
       (list)))
    (source (origin
              (method url-fetch)
              (uri (string-append
                       "https://www.cairographics.org/releases/pixman-"
                       version ".tar.gz"))
              (sha256
               (base32
                "02jnln2jg0m1g0835n2g1rf433haz609r00dwnz1dysqk0h2hp56"))))))

(define-public rofi-wayland-latest
  (let ((base rofi))
    (package
      (inherit rofi)
      (name "rofi-wayland")
      (version "5d4a3e14788945a9a1c7d18993151a3b00649373")
      (home-page "https://github.com/lbonn/rofi")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit (string-append "" version))
                      (recursive? #t)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0ahd6d30rkbzjcajghg5cwg09rq8bf70k62vni62jncc8dywbnws"))))
      (build-system meson-build-system)
      (inputs
       (modify-inputs (package-inputs base)
         (append wayland wayland-protocols)))
      (description
       (string-append
        (package-description base)
        "  This package, @code{rofi-wayland}, provides additional wayland
support.")))))

(define-public libdrm-latest
  (package
    (name "libdrm")
    (version "2.4.120")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://dri.freedesktop.org/libdrm/libdrm-"
                    version ".tar.xz"))
              (sha256
               (base32
                "0yijzgg6rdsa68bz03sw0lcfa2nclv9m3as1cja50wkcyxim7x9v"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags
       '(,@(match (%current-system)
             ((or "armhf-linux" "aarch64-linux")
              '("-Dexynos=true"
                "-Domap=true"
                "-Detnaviv=true"
                "-Dtegra=true"
                "-Dfreedreno-kgsl=true"))
             (_ '())))

       #:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (when tests?
                        (invoke "meson" "test" "--timeout-multiplier" "5")))))))
    (propagated-inputs
     (list libpciaccess))
    (native-inputs
     (list pkg-config))
    (home-page "https://dri.freedesktop.org/wiki/")
    (synopsis "Direct rendering userspace library")
    (description "The Direct Rendering Infrastructure, also known as the DRI,
is a framework for allowing direct access to graphics hardware under the
X Window System in a safe and efficient manner.  It includes changes to the
X server, to several client libraries, and to the kernel (DRM, Direct
Rendering Manager).  The most important use for the DRI is to create fast
OpenGL implementations providing hardware acceleration for Mesa.
Several 3D accelerated drivers have been written to the DRI specification,
including drivers for chipsets produced by 3DFX, AMD (formerly ATI), Intel
and Matrox.")
    (license license:x11)))
