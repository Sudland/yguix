(define-module (yg packages xorg)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages glib)
  #:use-module (yg packages gl)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages onc-rpc)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages video)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xdisorg)
  #:use-module (yg packages xdisorg)
  #:use-module (yg packages freedesktop)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix build-system waf)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1) #:hide (zip)))

(define-public xorg-server-xwayland
  (package
    (name "xorg-server-xwayland")
    (version "23.2.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.freedesktop.org/xorg/xserver")
             (commit (string-append "xwayland-"version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0z1diamp2s4dr4lkfzdc4gm4105aww45h8anxq1mh13n2x5ssl76"))))
    (inputs (list font-dejavu
                  libxcvt
                  dbus
                  egl-wayland
                  eudev
                  libfontenc
                  libdrm-latest
                  libepoxy-latest
                  libgcrypt
                  libtirpc
                  libxfont2
                  libxkbfile
                  pixman-latest
                  wayland-latest
                  wayland-protocols
                  xkbcomp
                  xkeyboard-config
                  xorgproto-latest
                  xtrans))
    (native-inputs (list pkg-config))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags
       (list "-Dxwayland_eglstream=true"
             (string-append "-Dxkb_dir="
                            (assoc-ref %build-inputs "xkeyboard-config")
                            "/share/X11/xkb")
             (string-append "-Dxkb_bin_dir="
                            (assoc-ref %build-inputs "xkbcomp") "/bin")
             ;; The build system insist on providing a default font path; give
             ;; that of dejavu, the same used for our fontconfig package.
             (string-append "-Ddefault_font_path="
                            (assoc-ref %build-inputs "font-dejavu")
                            "/share/fonts")
             "-Dxkb_output_dir=/tmp"
             (format #f "-Dbuilder_string=\"Build ID: ~a ~a\"" ,name ,version)
             "-Dxcsecurity=true"
             "-Ddri3=true"
             "-Dglamor=true"
             ;; For the log file, etc.
             "--localstatedir=/var")
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'patch-/bin/sh
                    (lambda _
                      (substitute* (find-files "." "\\.c$")
                        (("/bin/sh") (which "sh"))))))))

    (synopsis "Xorg server with Wayland backend")
    (description "Xwayland is an X server for running X clients under
Wayland.")
    (home-page "https://www.x.org/wiki/")
    (license license:x11)))

(define-public xorgproto-latest
  (package
    (name "xorgproto")
    (version "2023.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://xorg/individual/proto"
                                  "/xorgproto-" version ".tar.xz"))
              (sha256
               (base32
                "0b4c27aq25w1fccks49p020avf9jzh75kaq5qwnww51bp1yvq7xn"))))
    (build-system gnu-build-system)
    (propagated-inputs
     ;; To get util-macros in (almost?) all package inputs.
     (list util-macros))
    (home-page "https://cgit.freedesktop.org/xorg/proto/xorgproto")
    (synopsis "Xorg protocol headers")
    (description
     "This package provides the headers and specification documents defining
the core protocol and (many) extensions for the X Window System.")
    (license license:x11)))
