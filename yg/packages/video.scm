(define-module (yg packages video)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix build-system scons)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (yg packages xdisorg)
  #:use-module (gnu packages)
  #:use-module (gnu packages wm)
  #:use-module (yg packages linux)
  #:use-module (gnu packages wm)
  #:use-module (yg packages gl)
  #:use-module (gnu packages video)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (ice-9 match))


(define-public mpv-pipewire
  (package
    (inherit mpv)
    (name "mpv")
    (build-system meson-build-system)
    (native-inputs
     (list ; for zsh completion file
           python pkg-config))
    (arguments
     (list
      ;; No check function defined.
      #:tests? #f))
    (inputs (modify-inputs (package-inputs mpv)
              (prepend
                       pipewire)))))


(define-public mpvpaper
  (package
   (name "mpvpaper")
   (version "1.7")
   (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/GhostNaN/mpvpaper")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
         (base32 "07qkwxpg9f54gyvszy5fx4r1rabfk6g79q3pw93i3rcrrljas65q"))))
   (build-system meson-build-system)
   (native-inputs
    (list wlroots cmake pkg-config mpv))
   (home-page "https://github.com/GhostNaN/mpvpaper")
   (synopsis "Live wallpaper using mpv and wlroots")
   (description "MPVPaper is a wallpaper program for wlroots based wayland
  compositors, such as sway. That allows you to play videos with mpv as your wallpaper.")
   (license license:gpl3+)))
