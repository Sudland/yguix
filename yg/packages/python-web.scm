(define-module (yg packages python-web)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages time)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages bash)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (srfi srfi-1))

(define-public whoogle-search-latest
  (package
    (inherit whoogle-search)
    (name "whoogle-search")
    (version "0.8.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url
              "https://github.com/benbusby/whoogle-search")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "165x8jgd4glwmcfnpmssc2n7nkyxnc9xgh5gjr49sghqmm1w804q"))))
    (propagated-inputs
     (list python-attrs
           python-beautifulsoup4
           python-validators
           python-cachelib
           python-certifi
           python-cffi
           python-brotli
           python-chardet
           python-click
           python-cryptography
           python-cssutils
           python-defusedxml
           python-flask
           python-flask-session
           python-idna
           python-itsdangerous
           python-jinja2
           python-markupsafe
           python-more-itertools
           python-packaging
           python-pluggy
           python-py
           python-pycodestyle
           python-pycparser
           python-pyopenssl
           python-pyparsing
           python-pysocks
           python-dateutil
           python-requests
           python-soupsieve
           python-stem
           python-urllib3
           python-waitress
           python-wcwidth
           python-werkzeug
           python-dotenv))))


(define-public youtube-local
  (package
    (name "youtube-local")
    (version "2.8.9")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/user234683/youtube-local")
                    (commit
                     (string-append "v" version))))
              (file-name (git-file-name "v" version))
              (sha256
               (base32 "0ybbgwh8p6rlbkbmy7p4fwaczwz2mf3qv2zqmslnzzv7jfd3rz6v"))))
    (build-system copy-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-after 'install 'wrap-program
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/youtube-local")))
               (chmod bin #o755)
               (wrap-program bin
                `("GUIX_PYTHONPATH" ":" prefix (,(getenv "GUIX_PYTHONPATH"))))
               #t))))
       #:install-plan
       '(("youtube" "youtube")
         ("settings.py" "settings.py")
         ("server.py" "youtube-local"))))
    (inputs
     (list bash-minimal
           python
           python-stem
           python-gevent
           python-flask
           python-cachetools
           python-defusedxml))
    (home-page "https://github.com/user234683/youtube-local")
    (synopsis "browser-based client for watching Youtube anonymously and with greater page performance")
    (description "youtube-local is a browser-based client written in Python for watching Youtube anonymously and without the lag of the slow page used by Youtube. One of the primary features is that all requests are routed through Tor, except for the video file at googlevideo.com. This is analogous to what HookTube (defunct) and Invidious do, except that you do not have to trust a third-party to respect your privacy. The assumption here is that Google won't put the effort in to incorporate the video file requests into their tracking, as it's not worth pursuing the incredibly small number of users who care about privacy (Tor video routing is also provided as an option). Tor has high latency, so this will not be as fast network-wise as regular Youtube. However, using Tor is optional; when not routing through Tor, video pages may load faster than they do with Youtube's page depending on your browser.")
    (license license:agpl3)))
