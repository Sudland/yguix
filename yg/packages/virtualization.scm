(define-module (yg packages virtualization)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages virtualization)
  #:use-module (yg packages virtualization)
  #:use-module (yg packages glib)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match))



(define-public waydroid
  (package
    (name "waydroid")
    (version "1.4.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/waydroid/waydroid")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1cp528ybksgf1ifawygm2zqrxh870i264sqbk272775p4a36zlgx"))))
    (build-system python-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'build)                ; no setup.py
         (delete 'check)                ; no test suite
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (lib (string-append out "/lib/waydroid"))
                    (tools (string-append lib "/tools"))
                    (data (string-append lib "/data"))
                    (apps (string-append out "/share/applications"))
                    (bin (string-append out "/bin"))
                    (site (string-append out "/lib/python"
                                         ,(version-major+minor
                                           (package-version python))
                                         "/site-packages"))
                    (path (getenv "PYTHONPATH")))
               (mkdir-p tools)
               (mkdir-p data)
               (mkdir-p apps)
               (mkdir-p bin)
               (copy-recursively "tools" tools)
               (copy-recursively "data" data)
               (install-file (string-append data "/Waydroid.desktop")
                             (string-append apps))
               (substitute* (string-append apps "/Waydroid.desktop")
                 (("/usr") lib))
               (install-file "waydroid.py" lib)
               (symlink (string-append lib "/waydroid.py") (string-append bin
                                                                          "/waydroid.py"))
               (wrap-program (string-append bin "/waydroid.py"))))))))
    (inputs `(("bash-minimal" ,bash-minimal)
              ("dnsmasq" ,dnsmasq)
              ("libgbinder" ,libgbinder)
              ("lxc" ,lxc)
              ("nftables" ,nftables)
              ("python" ,python)
              ("python-gbinder" ,python-gbinder)
              ("python-dbus" ,python-dbus)
              ("python-pygobject" ,python-pygobject)))
    (home-page "https://waydro.id")
    (synopsis "Container-based approach to boot a full Android system")
    (description "Waydroid uses Linux namespaces @code{(user, pid, uts, net,
mount, ipc)} to run a full Android system in a container and provide Android
applications.  The Android inside the container has direct access to needed
underlying hardware.  The Android runtime environment ships with a minimal
customized Android system image based on LineageOS.  The used image is
currently based on Android 10.")
    (license license:gpl3)))
